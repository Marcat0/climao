# CLIMAX

# Documentação

## Sobre o Projeto

**Projeto que visa mineração de dadosos meterelógicos. Na qual busca de forma automatizada, dados confiáveis em sites/redes de meteorologia.**

Para acessar o MVP  [clique aqui.](https://gitlab.com/BDAg/climao/-/blob/master/MVP___Produto_M%C3%ADnimo_Vi%C3%A1vel_-_CLIMAX.pptx) 

Para acessar o cronograma [clique aqui.](https://gitlab.com/BDAg/climao/-/blob/master/Cronograma_-_CLIMAX.xlsx)

Para acessar o mapa de conhecimento [clique aqui.](https://gitlab.com/BDAg/climao/-/blob/master/Mapa_do_Conhecimento.png)

Para acessar a matriz de habilidade [clique aqui.]( https://gitlab.com/BDAg/climao/-/blob/master/Matriz_de_Habilidade_-_CLIMAX.xlsx)
## Andamento do Projeto

- [X] Entrega 01
- [X] Entrega 02
- [X] Entrega 03
- [X] Busca por fontes de informação na Webo (Sprint 1)
- [X] Desenvolvimento de WebBots e captura de dadoso (Sprint 2)
- [ ] Teste e validaçãoo (Sprint 3)
- [ ] Fechamento do Projeto

## Equipe

- Letícia Melo (SCRUM MASTER)

- Matheus Rezende (SCRUM MASTER)

- Felipe Marcato

- Rudi Alturria

- Samuel Canevari